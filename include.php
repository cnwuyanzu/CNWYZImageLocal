<?php
#注册插件
RegisterPlugin("CNWYZImageLocal", "ActivePlugin_CNWYZImageLocal");

function ActivePlugin_CNWYZImageLocal()
{
    Add_Filter_Plugin("Filter_Plugin_PostArticle_Succeed", "CNWYZImageLocal_handleImage");
}

function InstallPlugin_CNWYZImageLocal()
{
    global $zbp;
    if ($zbp->HasConfig('CNWYZImageLocal') == false) {
        $zbp->Config('CNWYZImageLocal')->filterUrl = "";
        $zbp->Config('CNWYZImageLocal')->cdnUrl = "";
        $zbp->SaveConfig('CNWYZImageLocal');
    }
}

function UninstallPlugin_CNWYZImageLocal()
{
    global $zbp;
    $zbp->DelConfig('CNWYZImageLocal');
}

/**
 * 图片本地化处理方法
 * Created by WuYanzu.
 * Author WuYanzu <9258405@qq.com>
 * Date 2022/1/6 14:34
 * @param BasePost $article
 * @throws Exception
 */
function CNWYZImageLocal_handleImage($article)
{
    require_once __DIR__ . "/class/CnwyzImageLocal.php";
    (new CnwyzImageLocal($article))->handleImageLocal();
}