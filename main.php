<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action='root';
if (!$zbp->CheckRights($action)) {$zbp->ShowError(6);die();}
if (!$zbp->CheckPlugin('CNWYZImageLocal')) {$zbp->ShowError(48);die();}

if (GetVars('act') == 'save' && count($_POST) > 0) {
    CheckIsRefererValid();
    $zbp->Config('CNWYZImageLocal')->filterUrl      = (string)GetVars("filterUrl");
    $zbp->Config('CNWYZImageLocal')->cdnUrl      = (string)GetVars("cdnUrl");
    $zbp->SaveConfig('CNWYZImageLocal');

    $zbp->SetHint('good');
    Redirect('./main.php');
}

$blogtitle='保存远程图片(本地化图片、CDN图片)';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
?>
<div id="divMain">
  <div class="divHeader">
      <?php echo $blogtitle;?>
  </div>
  <div class="SubMenu">
      <a href="main.php">
          <span class="m-left m-now">配置页面</span>
      </a>
      <a target="_blank" href="https://gitee.com/cnwuyanzu/zblog-plug-in-collection/blob/master/plugin/CNWYZImageLocal/index.md">
          <span class="m-right">帮助文档</span>
      </a>
  </div>
  <div id="divMain2">
<!--代码-->
      <form action="?act=save" method="post">
          <input type="hidden" name="csrfToken" value="<?php echo $zbp->GetCsrfToken(); ?>">
          <table width="100%" border="0">
              <tr height="32">
                  <th colspan="2" align="center">设置
                  </th>
              </tr>
              <tr height="32">
                  <td width="30%" align="left"><p><br/><b>· 过滤URL</b><br/>
                          <span class="note">&nbsp;&nbsp; 在此的URL都将不进行本地化 适用于CDN插件，<br/></span> <span class="note">&nbsp;&nbsp; 可留空如有多个请用 | 隔开</span></p></td>
                  <td>
                      <input id="filterUrl" name="filterUrl" type="text"
                             value="<?php echo $zbp->Config('CNWYZImageLocal')->filterUrl; ?>" size="100"/><br/>
                  </td>
              </tr>
              <tr height="32">
                  <td width="30%" align="left"><p><br/><b>· CND-URL</b><br/>
                          <span class="note">&nbsp;&nbsp; 可不填写(默认替换为网站域名)，<br/></span>
                          <span class="note">&nbsp;&nbsp; 填写之后替换为该域名<br/></span>
                          <span class="note">&nbsp;&nbsp; 域名必须带 / 结尾 ：例如:http://baidu.com/</span></p></td>
                  <td>
                      <input id="filterUrl" name="cdnUrl" type="text"
                             value="<?php echo $zbp->Config('CNWYZImageLocal')->cdnUrl; ?>" size="100"/><br/>
                  </td>
              </tr>
          </table>
          <hr/>
          <p>
              <input type="submit" value="提交" class="button"/>
          </p>
          <hr/>
      </form>
  </div>
</div>

<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>