# 保存远程图片(本地化、CDN、采集、存储)

## 基本信息

#### 付费插件售后QQ群

* 793577886(加群请带上订单号)
* 更多插件：[点击直达](https://gitee.com/cnwuyanzu/zblog-plug-in-collection)

#### 功能简介

1. 新建、编辑文章处理文章中非本站域名的附件下载到本地

#### 环境要求(安装说明)

1. <font color="red">PHP5.6</font>以上(必须)

#### 其他说明

1. 如果有的图片不能上传，请检查网站设置-》后台设置-》允许上传文件大小

2. 过滤URL设置(不需要填本网站域名)

    - 多个域名格式

      ```
      http://a.com|http://b.com
      ```

3. CDN域名配置

    - 必须带      ***/***     结尾 例如：

      ```
      http://a.com   #错误的
      http://a.com/  #正确的 
      ```

#### 配置截图

![](https://mengde-image.wolai.kim/note/20220111105632.png)